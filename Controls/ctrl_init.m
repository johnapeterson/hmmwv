dt_s = 0.0005;

AccelFilter_Hz = 3;
BdyAxOffset_g = 0;
BdyAyOffset_g = 0;
BdyAzOffset_g = 0;

YawRateIMU_Hz = 3;
RollRateIMU_Hz = 3;
PitchRateIMU_Hz = 3;
RatesDZ_degps = 2;

YawRateOffset_degps = 0;
RollRateOffset_degps = 0;
PitchRateOffset_degps = 0;

SuspOffsetEnable_bool = 0;
SuspOffsetFL_m = 0;
SuspOffsetFR_m = 0;
SuspOffsetRL_m = 0;
SuspOffsetRR_m = 0;

SprungCrnrAccelFilter_Hz = 5; 

SprungAccelOffsetFL_g = 0;
SprungAccelOffsetFR_g = 0;
SprungAccelOffsetRL_g = 0;
SprungAccelOffsetRR_g = 0;

UnSprungCrnrAccelFilter_Hz = 5;
UnsprungAccelOffsetFL_g = 0;
UnsprungAccelOffsetFR_g = 0;
UnsprungAccelOffsetRL_g = 0;
UnsprungAccelOffsetRR_g = 0;

Sinewave_Freq_hz = 2;
Sinewave_Amp_N = 100;
Sinewave_Input_On_bool = 0;

DisplSMStiffness_Npm = 20000;
VeloSMDamping_Npmps = 1000;
PitchStiffness_Npdeg = 5000;
PitchDamping_Npdegps = 500;
RollStiffness_Npdeg = 2000;
RollDamping_Npdegps = 1000;
RollAyGainEnable_bool = 0;

RollAyGainMap_x = [0 0.1];
RollAyGainMap_z = [0 0.1];

GrossRideGain = 0;

RideHiStaticFrcOn_bool = 0;
StaticFrcFL_N = 0;
StaticFrcFR_N = 0;
StaticFrcRL_N = 0;
StaticFrcRR_N = 0;

StaticFrcFL_N = 100;    %N 
StaticFrcFR_N = 100;    %N 
StaticFrcrRL_N = 100;    %N 
StaticFrcRR_N = 100;    %N 

AccelCalcDispl_bool = 0;
AngleCalcDispl_bool = 0;
ConstVx_bool = 0;
ConstVx_kph = 70;   %kph

rad2deg = 180/pi;
deg2rad = pi/180;
g2mps2 = 9.81;
kph2mps = 1/3.6;
mps22g = 1/9.81;

AngleSuspDisp_bool = 0;
AnglesEstFilter_Hz = 3;

Wheelbase_m = 3.1;  %m
TrackWidth_m = 2.1; %m

SuspDispl12_Hz = 12;    %Hz
SuspDispl8_Hz = 8;      %Hz

SMDisplFLCoeff_m2g = 1;  %m static ride height accel to m coeff
SMDisplFRCoeff_m2g = 1;
SMDisplRLCoeff_m2g = 1;
SMDisplRRCoeff_m2g = 1;
SMDisplFLOffset_m = 0.2;    %m static ride height
SMDisplFROffset_m = 0.2;
SMDisplRLOffset_m = 0.2;
SMDisplRROffset_m = 0.2;

JounceSoft_bool = 1;

YawRateHighThrshld_degps = 10;
YawRateLowThrshld_degps = 8;
HWRateHighThrshld_degps = 50;
HWRateLowThrshld_degps = 45;
VehSpdThrshld_kph = 25;
AxHighThrshld_g = 0.1;
AxLowThrshld_g = 0.08;
AyHighThreshold_g = 0.1;
AyLowThreshold_g = 0.08;

a_m = 0.45*Wheelbase_m;     %m distance from front axle to vehicle CG
b_m = Wheelbase_m - a_m;    %m distance from rear axle to vehicle CG
SMVeloLeakRate_mpss = 0.1;

SMDisplCoeff_m2g = 1;
SMDisplOffset_m = 1;
DisplVertCalcAccel_bool = 1;
SMDisplLeakRate_mps = 0.1;
DisplVertVeloCalcOn_bool = 1;
SMVeloCalcFilt_Hz = 3;

Fc_CalcWheelLongAccel = 10;
zeta_CalcWheelLongAccel = 2;

RideFrcCalcTimeOut_sec = 10;

Sinewave_Amp_N = 100; %N
SinewaveFreq_hz = 0.5;
SinewaveInputOn_bool = 0;







